local M = {}

M.init = function ()
  local has_orgmod, orgmod = pcall(require, 'orgmode')
  if not has_orgmod then
    return
  end

  -- Initialize orgmod
  orgmod.setup{}

  -- Add completions
  local has_compe, compe = pcall(require, 'compe')
  if has_compe then
    compe.setup({
      source = {
        orgmode = true
      }
    })
  end

end

return M
